from random import randint

a = [randint(0, 5000000) for x in range(500)]
b = [randint(0, 5000000) for x in range(50000)]

x = set(a) & set(b)

print(x)
