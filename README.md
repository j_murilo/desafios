# README #

#Teste Tecnico para estagio TrustCode

Primeiro desafio



Você recebe uma lista de número de 0 a 300 ordenados por ordem crescente.

>>lista = [x for x in range(300)]



Sua tarefa é achar um número aleatório informado pelo usuário da maneira mais eficiente possível.



Segundo desafio



Agora sua lista de 500 números não estão ordenados.

>>lista = [randint(0, 5000000) for x in range(500)]



E você recebe uma outra lista com 50000 números também aleatórios.

>>lista = [randint(0, 5000000) for x in range(50000)]



Sua tarefa é para cada item da segunda lista, imprimir quais destes números estão na primeira lista recebido.

Pontos para a solução mais rápida.





Terceiro desafio



Sua tarefa agora é dado um número qualquer imprimir todos os números primos até o número dado.



Quarto desafio



Neste problema você deve ler um conjunto de palavras, onde cada palavra é composta somente por letras no intervalo a-z e A-Z. Cada letra possui um valor específico, a letra a vale 1, a letra b vale 2 e assim por diante, até a letra z, que vale 26. Do mesmo modo, a letra A vale 27, a letra B vale 28 e a letra Z vale 52.