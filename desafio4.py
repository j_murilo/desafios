import string

alfa1 = {x:y for x,y in zip(string.ascii_lowercase,range(1,27))}
alfa2 = {x:y for x,y in zip(string.ascii_uppercase,range(27,53))}

alfa = {**alfa1, **alfa2}

x = input("Digite uma palavra: ")
valor = []


for i in x:
    y = alfa[i]
    valor.append(y)

soma = sum(valor)

print(valor)
print("A somma dos numeros correspondete a palavra é: ",sum(valor))

if soma > 1:
    for j in range(2, soma):
        if (soma % j == 0):
            print("Não é um numero primo")
            break
    else:
        print(soma, " É um numero primo.")
